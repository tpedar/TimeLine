//
//  UserController.swift
//  TimeLine
//
//  Created by Taylor Petersen on 11/3/15.
//  Copyright © 2015 Maverick. All rights reserved.
//

import Foundation

class UserController {
    
    let currentUser: User! = UserController.mockUsers().first
    
    static let sharedController = UserController()
    
    static func userForIdentifier(identifier: String, completion: (user: User?) -> Void) {
        completion(user: mockUsers().first)
    }

    static func fetchAllUsers(completion: (users: [User]) -> Void) {
        completion(users: mockUsers())
    }
    
    static func followUsers(user: User, completion: (succes: Bool) -> Void) {
        
    }
    
    static func userFollowUsers(user: User, userCheck: User, completion: (follow: Bool) -> Void) {
        
    }
    
    static func followedByUser(user: User, completion: (user: [User]?) -> Void) {
        completion(user: mockUsers())
    }
    
    static func authenticateUser(email: String, password: String, completion: (success: Bool, user: User?) -> Void) {
        completion(success: true, user: mockUsers().first)
    }
    
    static func createUser(email: String, username: String, password: String, bio: String?, url: String?, completion: (success: Bool, user: User?) -> Void) {
        completion(success: true, user: mockUsers().first)
    }
    
    static func updateUser(user: User, username: String, bio: String?, url: String?, completion: (success: Bool, user: User?) -> Void) {
        completion(success: true, user: mockUsers().first)
    }
    
    static func logOutCurrentUser() {
        
    }
    
    static func mockUsers() -> [User] {
        
        let user1 = User(username: "taylor", identifier: "1234")
        let user2 = User(username: "kelsi", identifier: "1235")
        let user3 = User(username: "maverick", identifier: "1236")
        
        return [user1, user2, user3]
    }

}
