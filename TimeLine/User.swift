//
//  User.swift
//  TimeLine
//
//  Created by Taylor Petersen on 11/3/15.
//  Copyright © 2015 Maverick. All rights reserved.
//

import Foundation


struct User: Equatable {
    let username: String
    let bio: String?
    let url: String?
    let identifier: String?
    
    init(username: String, bio: String? = nil, url: String? = nil, identifier: String) {
        
        self.username = username
        self.bio = bio
        self.url = url
        self.identifier = identifier
    }
}

func == (lhs: User, rhs: User) -> Bool {
    return lhs.username == rhs.username
}