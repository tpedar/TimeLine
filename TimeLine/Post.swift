//
//  Post.swift
//  TimeLine
//
//  Created by Taylor Petersen on 11/3/15.
//  Copyright © 2015 Maverick. All rights reserved.
//

import Foundation

struct Post: Equatable {
    let imageEndPoint: String
    let caption: String?
    let username: String
    let comments: [Comment]
    let likes: [Like]
    let identifier: String?
    
    init(imageEndPoint: String, caption: String?, username: String, comments: [Comment], likes: [Like], identifier: String? = nil) {
        
        self.imageEndPoint = imageEndPoint
        self.caption = caption
        self.username = username
        self.comments = comments
        self.likes = likes
        self.identifier = identifier
    }
}

func == (lhs: Post, rhs: Post) -> Bool {
    return (lhs.username == rhs.username) && (lhs.identifier == rhs.identifier)
}